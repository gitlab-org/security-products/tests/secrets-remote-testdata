### DO NOT REMOVE OR CHANGE CONTENTS UNLESS A CORRESPONDING MR EXIST IN SECRETS ANALYZER.

### ALL SECRETS IN THIS PROJECT ARE FAKE DUMMY SECRETS.

# Secrets Passthrough `git` and `url` Test

This repository is used for storing a remote ruleset that is loaded in tests of the [`secrets` analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/secrets) via a `git` or a `url` passthrough.

The configuration is stored in both:

- `main` branch: used by [`synthesize-git-passthrough`](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/tree/master/qa/fixtures/synthesize-git-passthrough) and [`synthesize-url-passthrough`](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/tree/master/qa/fixtures/synthesize-url-passthrough) fixtures in the analyzer.
- `config-with-extend` branch: used by [`extend-with-git-passthrough`](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/tree/master/qa/fixtures/extend-with-git-passthrough) and [`extend-with-url-passthrough`](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/tree/master/qa/fixtures/extend-with-url-passthrough) fixtures in the analyzer.

### Other Usages

The repository has a number of branches created to also support the [demos](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/#demos) created to demonstrate Pipeline Secret Detection's [customize rulesets feature](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/#customize-analyzer-rulesets).

- For demos in [Extend Default Ruleset](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/extend-default-ruleset): [`config-demos-extend`](https://gitlab.com/gitlab-org/security-products/tests/secrets-passthrough-git-and-url-test/-/tree/config-demos-extend) branch.
- For demos in [Replace Default Ruleset](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/replace-default-ruleset): [`config-demos-replace`](https://gitlab.com/gitlab-org/security-products/tests/secrets-passthrough-git-and-url-test/-/tree/config-demos-replace) branch.
- For demos in [Ignore Paths](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/ignore-paths): [`config-demos-ignore-paths`](https://gitlab.com/gitlab-org/security-products/tests/secrets-passthrough-git-and-url-test/-/tree/config-demos-ignore-paths) branch.
- For demos in [Ignore Patterns](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/ignore-patterns): [`config-demos-ignore-patterns`](https://gitlab.com/gitlab-org/security-products/tests/secrets-passthrough-git-and-url-test/-/tree/config-demos-ignore-patterns) branch.
- For demos in [Ignore Values](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/ignore-values): [`config-demos-ignore-values`](https://gitlab.com/gitlab-org/security-products/tests/secrets-passthrough-git-and-url-test/-/tree/config-demos-ignore-values) branch.
